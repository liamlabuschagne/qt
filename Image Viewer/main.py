#!/usr/bin/python3
import os
import sys
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import *

class Window(QtGui.QMainWindow):
	def __init__(self):
		super(Window,self).__init__()
		self.setGeometry(50,50,500,300)
		self.setWindowTitle("Image Viewer")
		self.setWindowIcon(QtGui.QIcon("icon.png"))

		#main menu
		quitAction = QtGui.QAction("&Quit",self)
		quitAction.setShortcut("Ctrl+Q")
		quitAction.setStatusTip("Leave the app")
		quitAction.triggered.connect(self.close_application)

		openAction = QtGui.QAction("&Open",self)
		openAction.setShortcut("Ctrl+O")
		openAction.setStatusTip("Open a image to view")
		openAction.triggered.connect(self.viewImage)

		self.statusBar()

		mainMenu = self.menuBar()
		fileMenu = mainMenu.addMenu("&File")
		fileMenu.addAction(openAction)
		fileMenu.addAction(quitAction)

		self.home()

	def home(self):
		# btn = QtGui.QPushButton("Quit",self);
		# btn.clicked.connect(self.close_application)
		# btn.resize(100,100)
		# btn.move(100,100)

		# self.textbox = QtGui.QLineEdit(self)
		# self.textbox.resize(300,20)
		# self.textbox.move(0,25)

		self.show()

	def viewImage(self):
		filePath = QtGui.QFileDialog.getOpenFileName(self, "Choose an image", "/home", "")
		label = QLabel(self)
		pixmap = QPixmap(filePath)
		label.setPixmap(pixmap)
		label.move(0,25)
		label.resize(pixmap.width(),pixmap.height())
		self.resize(pixmap.width(),pixmap.height()+25)
		label.show()
		self.show()

	def close_application(self):
		sys.exit()

def main():
	app = QtGui.QApplication(sys.argv)
	GUI = Window()
	sys.exit(app.exec_())

main()

# app = QtGui.QApplication(sys.argv)
# window = QtGui.QWidget()
# window.setGeometry(0,0,500,300)
# window.setWindowTitle("Test")
# window.show()
# app.exec_()