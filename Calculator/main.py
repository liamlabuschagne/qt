#!/usr/bin/python3
import os
import sys
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import *

class Window(QMainWindow):
	answer = None
	decimal = False
	decimalPlaces = 1

	numbers = [0]
	operators = []

	memory = 0

	def __init__(self):
		super(Window,self).__init__()
		self.setGeometry(0,50,200,375)
		self.setWindowTitle("Calculator")
		self.setWindowIcon(QtGui.QIcon("icon.png"))

		#menu
		quitAction = QAction("&Quit",self)
		quitAction.setShortcut("Ctrl+Q")
		quitAction.triggered.connect(self.quit)

		mainMenu = self.menuBar()
		fileMenu = mainMenu.addMenu("&File")
		fileMenu.addAction(quitAction)
		#end menu

		#display
		self.display = QLabel(self)
		self.display.resize(self.width(), 100)
		self.display.move(0,25)
		self.display.setStyleSheet("background-color: white;")
		#end display

		#buttons
		BUTTON_SIZE = 50

		ROW_ONE = 125
		ROW_TWO = ROW_ONE + 50
		ROW_THREE = ROW_TWO + 50
		ROW_FOUR = ROW_THREE + 50
		ROW_FIVE = ROW_FOUR + 50

		COL_ONE = 0
		COL_TWO = 50
		COL_THREE = 100
		COL_FOUR = 150

		# number buttons

		btnNum0 = QPushButton("0",self)
		btnNum0.move(COL_TWO,ROW_FOUR)
		btnNum0.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum1 = QPushButton("1",self)
		btnNum1.move(COL_ONE,ROW_ONE)
		btnNum1.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum2 = QPushButton("2",self)
		btnNum2.move(COL_TWO,ROW_ONE)
		btnNum2.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum3 = QPushButton("3",self)
		btnNum3.move(COL_THREE,ROW_ONE)
		btnNum3.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum4 = QPushButton("4",self)
		btnNum4.move(COL_ONE,ROW_TWO)
		btnNum4.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum5 = QPushButton("5",self)
		btnNum5.move(COL_TWO,ROW_TWO)
		btnNum5.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum6 = QPushButton("6",self)
		btnNum6.move(COL_THREE,ROW_TWO)
		btnNum6.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum7 = QPushButton("7",self)
		btnNum7.move(COL_ONE,ROW_THREE)
		btnNum7.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum8 = QPushButton("8",self)
		btnNum8.move(COL_TWO,ROW_THREE)
		btnNum8.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnNum9 = QPushButton("9",self)
		btnNum9.move(COL_THREE,ROW_THREE)
		btnNum9.resize(BUTTON_SIZE,BUTTON_SIZE)
		#end number buttons

		#operator buttons

		btnOperatorAdd = QPushButton("+",self)
		btnOperatorAdd.move(COL_FOUR,ROW_ONE)
		btnOperatorAdd.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnOperatorSubtract = QPushButton("-",self)
		btnOperatorSubtract.move(COL_FOUR,ROW_TWO)
		btnOperatorSubtract.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnOperatorMultiply = QPushButton("x",self)
		btnOperatorMultiply.move(COL_FOUR,ROW_THREE)
		btnOperatorMultiply.resize(BUTTON_SIZE,BUTTON_SIZE)

		btnOperatorDivide = QPushButton("÷",self)
		btnOperatorDivide.move(COL_FOUR,ROW_FOUR)
		btnOperatorDivide.resize(BUTTON_SIZE,BUTTON_SIZE)

		#end operator buttons

		#function buttons

		btnEquals = QPushButton("=",self)
		btnEquals.move(COL_THREE,ROW_FOUR)
		btnEquals.resize(BUTTON_SIZE,BUTTON_SIZE)
		btnEquals.clicked.connect(self.calculate)

		btnClear = QPushButton("C",self)
		btnClear.move(COL_ONE,ROW_FOUR)
		btnClear.resize(BUTTON_SIZE,BUTTON_SIZE)
		btnClear.clicked.connect(self.clear)

		btnM = QPushButton("M",self)
		btnM.move(COL_ONE,ROW_FIVE)
		btnM.resize(BUTTON_SIZE,BUTTON_SIZE)
		btnM.clicked.connect(self.readMemory)

		btnMAdd = QPushButton("M+",self)
		btnMAdd.move(COL_TWO,ROW_FIVE)
		btnMAdd.resize(BUTTON_SIZE,BUTTON_SIZE)
		btnMAdd.clicked.connect(self.memoryPlus)

		btnMMinus = QPushButton("M-",self)
		btnMMinus.move(COL_THREE,ROW_FIVE)
		btnMMinus.resize(BUTTON_SIZE,BUTTON_SIZE)
		btnMMinus.clicked.connect(self.memoryMinus)

		btnPoint = QPushButton(".",self)
		btnPoint.move(COL_FOUR,ROW_FIVE)
		btnPoint.resize(BUTTON_SIZE,BUTTON_SIZE)
		btnPoint.clicked.connect(self.addPoint)

		#end function buttons

		nums = [btnNum0,btnNum1,btnNum2,btnNum3,btnNum4,btnNum5,btnNum6,btnNum7,btnNum8,btnNum9]

		for btn in nums:
			btn.clicked.connect(self.setNumber)

		ops = [btnOperatorAdd, btnOperatorSubtract, btnOperatorMultiply, btnOperatorDivide]

		for op in ops:
			op.clicked.connect(self.setOperator)

		#end buttons

		self.show()

	def clear(self):
		self.display.setText("")
		self.answer = None
		self.operators = []
		self.numbers = [0]
		self.decimal = 0
		self.decimalPlaces = 1

	def displayEquation(self):
		print(len(self.numbers))
		# the user has pressed equals
		if(self.answer != None):
			self.display.setText( "= " + str(round(self.answer,self.decimalPlaces)))

		else:
			textToDisplay = ""
			
			oplen = len(self.operators)
			if(oplen > 0):
				operator = self.operators[oplen-1]
				if(self.decimalPlaces > 1):
					textToDisplay = operator + " " + str(round(self.numbers[ len(self.numbers)-1 ],self.decimalPlaces))
				elif(self.decimal):
					textToDisplay = operator + " " + str(int(self.numbers[ len(self.numbers)-1 ])) + "."
				else:
					textToDisplay = operator + " " + str(int(self.numbers[ len(self.numbers)-1 ]))
			else:
				if(self.decimalPlaces > 1):
					textToDisplay = str(round(self.numbers[ len(self.numbers)-1 ],self.decimalPlaces))
				elif(self.decimal):
					textToDisplay = str(int(self.numbers[ len(self.numbers)-1 ])) + "."
				else:
					textToDisplay = str(int(self.numbers[ len(self.numbers)-1 ]))

			if(self.numbers[ len(self.numbers)-1 ] != 0):
				self.display.setText(textToDisplay)


	def setNumber(self):
		sender = self.sender()

		n = self.numbers[len(self.numbers)-1]
		k = float(sender.text()) 
		if(self.decimal):
			for i in range(self.decimalPlaces):
				k /= 10
			self.decimalPlaces += 1
			n += k
		else:
			n *= 10
			n += k
		
		self.numbers[len(self.numbers)-1] = n

		self.displayEquation()

	def setOperator(self):
		sender = self.sender()

		self.operators.append(sender.text())
		self.decimal = False
		self.decimalPlaces = 1
		self.numbers.append(0)
		
		self.displayEquation()

	def addPoint(self):
		self.decimal = True
		self.displayEquation()

	def calculate(self):
		self.answer = 0
		count = 0
		for n in self.numbers:
			if(count == 0):
				self.answer = n
			else:
				if(  self.operators[count-1] == "+"):
					self.answer += n
				elif(self.operators[count-1] == "-"):
					self.answer -= n
				elif(self.operators[count-1] == "x"):
					self.answer *= n
				elif(self.operators[count-1] == "÷"):
					self.answer /= n
			count+=1

		self.displayEquation()

	def memoryPlus(self):
		if(self.answer != None):
			self.memory += self.answer

	def memoryMinus(self):
		if(self.answer != None):
			self.memory -= self.answer

	def readMemory(self):
		self.numbers[ len(self.numbers)-1 ] = self.memory
		self.displayEquation()

	def quit(self):
		sys.exit()

def main():
	app = QApplication(sys.argv)
	GUI = Window()
	sys.exit(app.exec_())

main()